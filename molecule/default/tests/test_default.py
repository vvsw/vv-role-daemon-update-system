import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_lxd_network(host):
	cmd = host.run("ip address")
	print(cmd.stdout)
	assert 'true'

def test_lxd_remote(host):
	cmd = host.run("/opt/lxd/bin/lxc remote list")
	print(cmd.stdout)
	cmd = host.run("ip address")
	print(cmd.stdout)
	assert 'ubuntu-minimal' in cmd.stdout